import SelectOption from  './SelectOption'

/**
 * Creates a SelectOption object with given parameters.
 * @param {String} label - The label of the select2 option.
 * @param {String} value - The value of the select2 option.
 * @returns {SelectOption}
 */
export default function (label, value) {
  return new SelectOption(label, value)
}
