/**
 * Describes the select2 input option.
 */
class SelectOption {
  /**
   * Creates a select2 option.
   * @param {String} label - The label of the select2 option.
   * @param {String} value - The value of the select2 option.
   */
  constructor (label, value) {
    this.label = label
    this.value = value
  }
}

export default SelectOption