/**
 * The MetaWeather API calls list.
 */
export default {
  woeIdApi: 'https://www.metaweather.com/api/location/search',
  weatherDetailsApi: 'https://www.metaweather.com/api/location'
}