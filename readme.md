# Vue weather app
Simple weather app that shows temperatures chart using [Meta Weather API](https://www.metaweather.com/api/)

## Installation

### Requirements
* Node ^v10.9.0
* NPM ^v6.2.0

### Getting the project

* `git clone https://kacperorzechowski@bitbucket.org/kacperorzechowski/rtbhouse.git`
* `cd rtbhouse`

### Setting up and run
* `npm install` installs all packages
* `npm run dev` starts the development server
* `npm run build` builds static files for deployment purpose


## License
[MIT](https://choosealicense.com/licenses/mit/)